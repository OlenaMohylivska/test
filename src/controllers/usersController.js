const express = require('express');
const router = express.Router();

const {
  getUserInfo,
  deleteUserProfile,
  updateUserProfilePassword
} = require('../services/usersService');

router.get('/me', async (req, res) => {
  const {userId} = req.user;
  const info = await getUserInfo(userId);
  if(info._id) {
    res.json({user: {
    '_id': info._id,
    'username': info.username,
    createdDate: info.createdDate
  }})
  } else {
    res.status(400).json({message: 'No info'})
  }
})

router.delete('/me', async (req, res) => {
  const {userId} = req.user;

  try {
    await deleteUserProfile(userId);

    res.status(200).json({message: 'Success'});
  } catch (err) {
    res.json({message: 'string'});
  }
});

router.patch('/me', async (req, res) => {
  const {userId} = req.user;
  const {oldPassword, newPassword} = req.body;
  
  if (!userId || !newPassword) {
    return res.status(400).json({message: 'string'});
  }
  
  const result = await updateUserProfilePassword(userId, newPassword, oldPassword);

  if(!result) {
    return res.status(400).json({message: 'bad'});
  }

  return res.status(200).json({message: 'good'});
});

module.exports = {
  usersRouter: router
}