const express = require('express');
const router = express.Router();

const {
    register,
    login
} = require('../services/authService');

router.post('/register', async (req, res) => {
    try {
        const {
            username,
            password
        } = req.body;

        await register({username, password});

        res.json({message: 'Account created successfully!'});
    } catch (err) {
        res.status(400).json({message: err.message});
    }
});

router.post('/login', async (req, res) => {
    try {
        const {
            username,
            password
        } = req.body;

        const token = await login({username, password});
        res.json({jwt_token: token, message: 'Logged in successfully!'});
    } catch (err) {
        res.status(400).json({message: err.message});
    }
});

module.exports = {
    authRouter: router
}