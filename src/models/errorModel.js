const mongoose = require('mongoose');

const Error = mongoose.model('Error', {
  message: String
})

module.exports = { Error }