const mongoose = require('mongoose');

const Credentials = mongoose.model('Credentials', {
  username: String,
  password: String
});

module.exports = { Credentials };