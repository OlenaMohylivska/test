const bcrypt = require('bcrypt');

const {User} = require('../models/userModel');

const getUserInfo = async (userId) => {
  const userInfo = await User.findOne({_id: userId})
  return userInfo
}

const deleteUserProfile = async (userId) => {
  await User.findOneAndRemove({_id: userId});
};

const updateUserProfilePassword = async (userId, newPassword, oldPassword) => {
  console.log('worj');
  const user = await User.findOne({_id: userId});
  const compared = await bcrypt.compare(oldPassword, user.password);
  
  if(!compared) {
    return false
  }
 
  await User.findOneAndUpdate({_id: userId}, {password: await  bcrypt.hash(newPassword, 10)});
  return true;
};

module.exports = {
  getUserInfo,
  deleteUserProfile,
  updateUserProfilePassword
}